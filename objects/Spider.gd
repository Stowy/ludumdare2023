extends Node3D

signal spider_arrived

#@export var health: int = 100
@export var speed: float = 1.2

var begin_position = Vector3(0, 0, 0)
var target_position = Vector3(0, 0, 0)
var direction

#var weight: float  = 0.0
#var is_done: bool = false

@onready var monster_manager = "../Terrain/MonsterManager"


func _ready():
	begin_position = self.position
	direction = (target_position - begin_position).normalized()


func _process(delta):
	self.position += direction * speed * delta

	var distance = (target_position - self.position).length_squared()
	if abs(distance) <= 0.0001:
		spider_arrived.emit()
		queue_free()
