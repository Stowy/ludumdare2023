extends Node3D


class Segment:
	var begin: Vector3 = Vector3(0, 0, 0)
	var end: Vector3 = Vector3(0, 0, 0)

	func _init(v1, v2):
		begin = v1
		end = v2

	func get_size() -> float:
		return (begin - end).length()


@export var min_limit: Vector3 = Vector3(-16, -16, 0)
@export var max_limit: Vector3 = Vector3(16, 16, 0)
@export var time_period = 5.0  # 5000ms
@export var web_mat: StandardMaterial3D
@export var spider: Resource
@export var spider_spawn_height: float = 1.0

var time = 0
var segments: Array[Segment]
#var new_position: Vector3 = Vector3(0, 0, 0)
var mouse_line: MeshInstance3D


# Called when the node enters the scene tree for the first time.
func _ready():
	# TODO : Compute automatically
	segments.append(
		Segment.new(Vector3(min_limit.x, 0, min_limit.y), Vector3(min_limit.x, 0, max_limit.y))
	)
	segments.append(
		Segment.new(Vector3(min_limit.x, 0, max_limit.y), Vector3(max_limit.x, 0, max_limit.y))
	)
	segments.append(
		Segment.new(Vector3(max_limit.x, 0, max_limit.y), Vector3(max_limit.x, 0, min_limit.y))
	)
	segments.append(
		Segment.new(Vector3(max_limit.x, 0, min_limit.y), Vector3(min_limit.x, 0, min_limit.y))
	)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time += delta
	if time > time_period:
		spawn_spider()
		# Reset timer
		time = 0


func on_spider_arrival(
	begin_segment_idx: int,
	end_segment_idx: int,
	new_position: Vector3,
	target: Vector3,
	#	normal: Vector3,
	last_point: Vector3
):
	segments[begin_segment_idx].begin = new_position
	segments[end_segment_idx].end = new_position

	#temp vars for line visualisation
	var temp_pos = new_position
	temp_pos.y = 0.1
	var temptarget = target
	temptarget.y = 0.1
#
#	#draw function (cut line)
#	mouse_line = Draw3d.line(temp_pos, temptarget, Color.WHITE)
#	var mid_direction: Vector3 = lerp(new_position, target, 0.5)
#
#	#draw normal (red)
#	normal.y = 0.1
#	mouse_line = Draw3d.line(mid_direction, normal, Color.RED)

	last_point.y = 0.1
	generate_triangle(temptarget, temp_pos, last_point)


func select_random_segment() -> int:
	var total_size: float = 0.0
	var sizes: PackedFloat32Array = []
	for segment in segments:
		var size: float = segment.get_size()
		sizes.append(size)
		total_size += size

	var wall_selection: float = randf_range(0.0, total_size)

	var size_buffer = 0.0
	for segment_id in range(0, segments.size()):
		var size = sizes[segment_id]
		size_buffer += size
		if wall_selection <= size_buffer:
			return segment_id

	assert(false)
	return 0


func spawn_spider():
	var new_spider = spider.instantiate()

	#Get random point on segment
	var segment_idx = select_random_segment()
	var segment = segments[segment_idx]
	var new_position = get_random_point_on(segment, 0.4, 0.6)

	#select previous or next segment
	var neighbour_selector = randi_range(0, 1) == 0
	var second_segment_idx
	var target

	#drawing purposes
	var last_point
#	var normal

	var begin_index: int
	var end_index: int

	if neighbour_selector:
		second_segment_idx = segment_idx - 1
		target = segments[second_segment_idx].begin
		last_point = segments[second_segment_idx].end
		begin_index = segment_idx
		end_index = second_segment_idx
#		normal = Vector3(target.z - new_position.z, 0, -(target.x - new_position.x))
	else:
		second_segment_idx = (segment_idx + 1) % segments.size()
		target = segments[second_segment_idx].end
		last_point = segments[second_segment_idx].begin
		begin_index = second_segment_idx
		end_index = segment_idx
#		normal = Vector3(-(target.z - new_position.z), 0, target.x - new_position.x)

	new_spider.position = Vector3(new_position.x, spider_spawn_height, new_position.z)
	new_spider.target_position = target
	new_spider.spider_arrived.connect(
		on_spider_arrival.bind(begin_index, end_index, new_position, target, last_point)
	)
	add_child(new_spider)


func get_random_point_on(segment: Segment, min_val: float, max_val: float):
	var point_a = segment.begin
	var point_b = segment.end
	var random_value = randf_range(min_val, max_val)
	var point_c = lerp(point_a, point_b, random_value)
	return point_c


func generate_triangle(v1, v2, v3):
	var vertices = PackedVector3Array()
	vertices.push_back(v1)
	vertices.push_back(v2)
	vertices.push_back(v3)
	vertices.push_back(v3)
	vertices.push_back(v2)
	vertices.push_back(v1)

	var uvs = PackedVector2Array()
	uvs.push_back(Vector2(v1.x, v1.z) / 10.0)
	uvs.push_back(Vector2(v2.x, v2.z) / 10.0)
	uvs.push_back(Vector2(v3.x, v3.z) / 10.0)
	uvs.push_back(Vector2(v3.x, v3.z) / 10.0)
	uvs.push_back(Vector2(v2.x, v2.z) / 10.0)
	uvs.push_back(Vector2(v1.x, v1.z) / 10.0)
	for i in range(0, uvs.size()):
		uvs[i].x += 0.5
		uvs[i].y += 0.5

	# Initialize the ArrayMesh.
	var arr_mesh = ArrayMesh.new()
	var arrays = []
	arrays.resize(Mesh.ARRAY_MAX)
	arrays[Mesh.ARRAY_VERTEX] = vertices
	arrays[Mesh.ARRAY_TEX_UV] = uvs

	# Create the Mesh.
	arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)

	arr_mesh.surface_set_material(0, web_mat)

	var m = MeshInstance3D.new()
	m.mesh = arr_mesh
	
	var shape = CollisionShape3D.new()
	# Rajouter les vertexs
	
	var area = Area3D.new()
	area.add_child(shape)
	
	m.add_child(area)

	add_child(m)


func get_triangle_center(v1: Vector3, v2: Vector3, v3: Vector3):
	return (v1 + v2 + v3) / 3.0


func calculate_area(v1: Vector3, v2: Vector3, v3: Vector3):
	var len_a = (v2 - v1).length()
	var len_b = (v3 - v2).length()
	var len_c = (v1 - v3).length()

	var semi_permimeter = (len_a + len_b + len_c) / 2

	var triangle_area = sqrt(
		(
			semi_permimeter
			* (semi_permimeter - len_a)
			* (semi_permimeter - len_b)
			* (semi_permimeter - len_c)
		)
	)

	return triangle_area
