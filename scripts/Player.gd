extends CharacterBody3D

@export_subgroup("Components")
@export var view: Node3D

@export_subgroup("Properties")
@export var acceleration: float = 80.0
@export var deceleration: float = 10.0
@export var max_speed: float = 8.0

@onready var animation_tree = $AnimationTree


func _physics_process(delta: float):
	handle_movement(delta)
	var velocity_ratio = velocity.length() / max_speed

	animation_tree.set("parameters/BlendTree/Blend2/blend_amount", velocity_ratio)

	if Input.is_action_just_pressed("attack"):
		animation_tree.get("parameters/playback").travel("attack")


func handle_movement(delta: float):
	var input := Vector3.ZERO

	input.x = Input.get_axis("move_left", "move_right")
	input.z = Input.get_axis("move_up", "move_down")

	input = input.rotated(Vector3.UP, view.rotation.y).normalized()
	var velocity_increment = input * acceleration * delta

	velocity += velocity_increment
	var speed = velocity.length()
	if speed > max_speed:
		velocity /= speed
		velocity *= max_speed

	if velocity_increment.length_squared() == 0:
		var slowdown = -velocity * deceleration * delta
		velocity += slowdown

	move_and_slide()

	var rotation_direction: float = 0.0
	if Vector2(velocity.z, velocity.x).length_squared() > 0:
		rotation_direction = Vector2(velocity.z, velocity.x).angle()

	rotation.y = rotation_direction
